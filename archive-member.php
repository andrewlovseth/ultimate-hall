<?php get_header(); ?>


    <section class="page-header align-center grid" id="top">
        <h1>Hall of Famers</h1>
    </section>

    <?php get_template_part('templates/archive-member/sub-nav'); ?>

    <?php get_template_part('templates/archive-member/classes'); ?>


    <div class="back-to-top">
        <a href="#page" class="smooth">Back to Top</a>
    </div>


<?php get_footer(); ?>