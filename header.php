<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	
	<header class="site-header grid">
		<?php get_template_part('template-parts/header/logo'); ?>

		<?php get_template_part('template-parts/header/hamburger'); ?>

		<?php get_template_part('template-parts/header/navigation'); ?>
	</header>

	<main class="site-content<?php if(get_field('hero_image')): ?> has-hero<?php endif; ?>">