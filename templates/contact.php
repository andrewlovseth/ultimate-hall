<?php 

/*
  
    Template Name: Contact
*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero-image'); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/contact/contact-info'); ?>

    <?php get_template_part('templates/contact/donate'); ?>

<?php get_footer(); ?>