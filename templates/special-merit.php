<?php 

/*

    Template Name: Special Merit
    Template Post Type: member

*/

get_header(); ?>
    
    <?php get_template_part('templates/special-merit/introduction'); ?>

    <?php get_template_part('templates/special-merit/entries'); ?>
    
    <?php get_template_part('templates/single-member/gallery'); ?>
    
<?php get_footer(); ?>

