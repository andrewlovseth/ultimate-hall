	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/header/logo'); ?>
		
		<?php get_template_part('template-parts/footer/navigation'); ?>
	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>