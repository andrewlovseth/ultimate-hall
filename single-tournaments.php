<?php get_header(); ?>

    <?php get_template_part('templates/single-tournaments/page-header'); ?>

    <?php get_template_part('templates/single-tournaments/members'); ?>

    <?php get_template_part('templates/single-tournaments/other-tournaments'); ?>

    <?php //get_template_part('templates/single-tournaments/years'); ?>

<?php get_footer(); ?>